package com.training.covid.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.training.covid.entity.Center;

@RestController
public class HomeController {

	List<Center> center = new ArrayList<Center>();
	
	public HomeController() {
		center.add(new Center(400001, "Mumbai"));
		center.add(new Center(412105, "Pune"));
		center.add(new Center(416406, "Sangali"));
	}


	@GetMapping("/allCenters")
	public ResponseEntity<?> findcenters() {
		return ResponseEntity.ok(center);
	}
	
	@GetMapping("/allCenters/{pin}")
	public ResponseEntity<?> findcenter(@PathVariable("pin") int pin) {
		List<Center> list = new ArrayList<Center>();
		for (Center c : center) {
			if(c.getPincode() == pin) {
				list.add(c);
			}
		}
		return ResponseEntity.ok(list);
	}
	
	@PostMapping("/covidCenter")
	public ResponseEntity<?> addcenters(@RequestBody Center c) {
		center.add(c);
		return ResponseEntity.ok("Center added successfully");
	}
}
