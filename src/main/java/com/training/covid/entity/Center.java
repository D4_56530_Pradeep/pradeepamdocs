package com.training.covid.entity;

public class Center {

	private int pincode;
	private String centerName;
	
	
	public Center(int pincode, String centerName) {
		super();
		this.pincode = pincode;
		this.centerName = centerName;
	}


	public int getPincode() {
		return pincode;
	}


	public void setPincode(int pincode) {
		this.pincode = pincode;
	}


	public String getCenterName() {
		return centerName;
	}


	public void setCenterName(String centerName) {
		this.centerName = centerName;
	}


	@Override
	public String toString() {
		return "Center [pincode=" + pincode + ", centerName=" + centerName + "]";
	}
	
	
}
